<?php declare(strict_types=1);

namespace Database\Factories;

use App\Models\CommentModel;
use Illuminate\Database\Eloquent\Factories\Factory;
class CommentModelFactory extends Factory
{
	protected $model = CommentModel::class;

	public function definition(): array
    {
		return [

		];
	}
}
