<?php declare(strict_types=1);

namespace Database\Factories;

use App\Models\BlogModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogModelFactory extends Factory
{
	protected $model = BlogModel::class;

	public function definition(): array
    {
		return [

		];
	}
}
