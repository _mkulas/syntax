<?php declare(strict_types=1);

namespace Database\Factories;

use App\Models\UserModel;
use Illuminate\Database\Eloquent\Factories\Factory;
class UserModelFactory extends Factory
{
	protected $model = UserModel::class;

	public function definition(): array
    {
		return [

		];
	}
}
