<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
class CommentModel extends Model
{
    use HasFactory;

    protected $table = 'comments';

    protected $fillable = [
        'id',
        'parent_id',
        'blog_id',
        'user_id',
        'text',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            UserModel::class,
            'user_id',
            'id'
        );
    }

    public function blog(): BelongsTo
    {
        return $this->belongsTo(
            BlogModel::class,
            'blog_id',
            'id'
        );
    }

    public function parent(): BelongsTo // NADRADENÝ
    {
        return $this->belongsTo(
            CommentModel::class,
            'parent_id'
        );
    }

    public function replies(): HasMany // PODRADENÝ
    {
        return $this->hasMany(
            CommentModel::class,
            'parent_id'
        );
    }
}
